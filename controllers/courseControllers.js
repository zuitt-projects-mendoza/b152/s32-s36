//import Course Model
const Course = require("../models/Course");

module.exports.addCourse = (req,res) => {

	//Does this controller receive user input?
	//Yes.
	//Where or what can we access to get the user input?
	//req.body
	console.log(req.body);


	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	newCourse.save()
	.then(course => res.send(course))
	.catch(error => res.send(error))


}

module.exports.getAllCourses = (req,res) => {

	//find all documents in the Course collection
	//We will use the find() method of our Course model
	//Because the Course model is connected to our courses collection
	//Course.find({}) = db.courses.find({})
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

//get single course
module.exports.getSingleCourse = (req,res) => {

	//console.log(req.params.id)//Check the if you can receive the data coming from the url.

	//let id = req.params.id

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

//update course: name, description and price
module.exports.updateCourse = (req,res) => {

	//console.log(req.params.id);//Where can we get our course's id?
	//console.log(req.body)//contains our new values.

	//updates object will contain the field/fields to update and its new value.
	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	}

	//use findByIdAndUpdate() to update our course
	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

}

module.exports.archive = (req,res) => {

	//console.log(req.params.id);//course's id

	//updates object will contain the field/fields to update and its new value.
	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

}

module.exports.activate = (req,res) => {

	//console.log(req.params.id);//course's id

	//updates object will contain the field/fields to update and its new value.
	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

}

module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.getInactiveCourses = (req,res) => {

	Course.find({isActive:false})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.findCoursesByName = (req,res) => {

	//console.log(req.body)//contain the name of the course you are looking for.

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		//find returns [] if there is no document that matches the criteria
		if(result.length === 0){
			return res.send("No Courses Found.")
		} else {
			return res.send(result)
		}

	})
}

module.exports.findCoursesByPrice = (req,res) => {


	//console.log(req.body)

	Course.find({price: req.body.price})
	.then(result => {

		if(result.length === 0){
			return res.send("No Course Found.")
		} else {
			return res.send(result)
		}


	})
	.catch(err => res.send(err));


}

module.exports.getEnrollees = (req,res) => {

	Course.findById(req.user.id)
	.then(course => res.send(course.enrollees))
	.catch(err => res.send(err));

}