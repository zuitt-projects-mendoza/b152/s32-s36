//import User Model
const User = require("../models/User");

//import Course Model
const Course = require("../models/Course");

//import bcrypt
const bcrypt = require("bcrypt");
//bcrypt is a package which will help us add a layer of security for our user's passwords.

//auth module
const auth = require("../auth");
//console.log(auth);//this is our own auth js module.

module.exports.registerUser = (req,res) => {

	//Does this controller need user input?
	//yes.
	//How/Where can we get our user's input?
	//req.body
	console.log(req.body);

	if(req.body.password.length < 8){
		return res.send("PW is short")
	}

	/*
		bcrypt adds a layer of security to our user's password.

		What bcrypt does is hash our password into a randomized character version of the original string.

		It is able to hide your password within that randomized string.
		
		syntax:
		bcrypt.hashSync(<stringToBeHashed>,<saltRounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized

	*/

	//create a variable to store the hashed/encrypted password. Then this new hashed pw will be what save in our database.
	const hashedPW = bcrypt.hashSync(req.body.password,10);
	//console.log(hashedPW);//IF you want to check if the password was hashed.

	//Create a new user document out of our User model:
	let newUser = new User({

		//Check your model, check the fields you need in your new document.
		//Check your req.body if you have all the values needed for each field.
		//Add hashedPW to add the hashed password in the new document.
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW

	});

	//newUser is our new document created out of our User model.
	//new documents created out of the User model have access to some methods.
	//.save() method will allow us to save our new document into the collection that our models is connected to.
	//note: do not add a semicolon to your then() chain
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));


}

module.exports.getAllUsers = (req,res) => {

	//User.find({}) = db.users.find({})
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

module.exports.loginUser = (req,res) => {

	//Does this controller need user input?
	//yes
	//Where can we get this user input?
	//req.body
	console.log(req.body);//contains user credentials

	/*
		1. find the user by the email.
		2. If we found a user, we will check his password.
		3. If we don't find a user, we will send a message to the client.
		4. If upon checking the found user's password is the same as our input password, we will generate a "key" to access our app. If not, we will turn him away by sending a message in the client.
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {

		//console.log(foundUser);//found document: It's an object.
		if(foundUser === null){
			return res.send("No User Found.")
		} else {
			//if we find a user, the foundUser parameter, will contain the details of the document that matched our req.body.email
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			console.log(isPasswordCorrect);
			/*
				syntax:
				bcrypt.compareSync(<string>,<hashedString>)

				"spidermanOG"
				"$2b$10$ejEI0sqoMtvfdPpTfpzjjewB7525F0swxpXvatMD6nvS60WVTmVbm"

				IF the string and the hashedString matches, compareSync method from bcrypt, will return true.

				If not, it will return false.

			*/
			if(isPasswordCorrect){
				/*
					auth.createAccessToken will receive our foundUser as an argument. Then, in the createAccessToken() will return a "token" or "key" which contains some of our user's details.

					This key is encoded and only our own methods will be able to decode it.

					auth.createAccessToken will return a JWT token out of our user details. Then, with res.send() we will be able to send the token to our client.

					This token or key will now let us allow or disallow users to access certain features of our app depending if they admins or non-admins.
				*/
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("Incorrect Password.")
			}
		}

	})
	.catch(err => res.send(err));

}

//allow us to get the details of our logged in user
module.exports.getUserDetails = (req,res) => {

	/*
		req.user contains details from our decodeToken because the route for this controller uses verify as a middleware. Routes that do not use verify will not have access to req.user.
	*/

	console.log(req.user);

	//Find our logged in user's document from our db and send it to the client by its id
	//Model.findById() will allow us to look for a document using an id
	//Model.findById() = db.collection.findOne({_id: "id"})
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
	//res.send("testing for verify");

}

module.exports.checkEmailExists = (req,res) => {

	//console.log(req.body.email);//check if you can receive the email from our client's request body.
	
	//You can use find({email: req.body.email}), however, find() returns multiple documents in an ARRAY.

	//SPIDERMAN3@gmail.com is not the same as spiderman3@gmail.com

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)

		if(result === null){
			return res.send("Email is available.");
		} else {
			return res.send("Email is already registered.")
		}

	})
	.catch(err => res.send(err));
}

module.exports.updateUserDetails = (req,res) => {

	console.log(req.body)//input for new values for our user's details.
	console.log(req.user.id)//check the logged in user's id

	//What method can we use to update our user's details?
	//findByIdAndUpdate

	let updates = {

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo

	}

	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
}

module.exports.updateAdmin = (req,res) => {

	//console.log(req.user)//WE can still have req.user because we are passing a token. HOWEVER, the details in req.user will be the admin's because you are passing the admin's token.
	console.log(req.params.id)//id of the user we want to update.

	/*
		Mini-activity

		The selected user's id is passed through req.params.id

		Use a method from our User model which will allow us to find the document by its id and update our user to an admin.

		Directly update the user to an admin. Similar to archive/activate.

		Then, send the updated User's details to our client.
		Catch the error and send it to our client.

		Take a screenshot of the response in your client and send it to the hangouts

	*/

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

}

//enroll user

module.exports.enroll = async (req,res) => {
	//console.log("test enroll route")

	/*
		Enrollment Steps:

		1. Look for the user by its id.
			-push the details of the course we're trying to enroll in. We'll push to a new enrollment subdocument in our user.

		2. Look for the course by its id.
			-push the details of the enrollee/user who's trying to enroll. We'll push to a new enrollees subdocument in our course.

		3. When both saving of documents are successful, we send a message to the client.

	*/

	console.log(req.user.id)//the user's id from the decoded token after verify()
	console.log(req.body.courseId)//the course from our request body

	/*
		Mini-Activity

			Inside our enroll controller:

			Add a check or check if the logged in user is an admin or not.
			If he is an admin, return a res.send() to the client with the following message:
			"Action Forbidden."

	*/
	//console.log(req.user)
	//get the details of the user who's trying to enroll in req.user
	/*
			req.user 
				{
					id,
					email,
					isAdmin
				}

	*/

	//process stops here and sends response IF user is an admin
	if(req.user.isAdmin) {
		return res.send("Action Forbidden")
	}

	//If not, we continue to our next steps:

	//Find the user:

	/*
		async - async keyword allows us to make our function asynchronous. Which means, that instead of JS regular behaviour of running each code line by line, It will allow us to wait for the result of functions.

		To wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish before proceeding.

	*/

	//return a boolean to our isUserUpdated variable as a result of our query and after saving our courseId into our user's enrollment subdocument array
	let isUserUpdated = await User.findById(req.user.id).then(user => {

		//check if you found the user's document
		//console.log(user);

		//Add the courseId in an object and push that object into the user's enrollments array:
		let newEnrollment = {
			courseId: req.body.courseId
		}

		//access the enrollments array from our user and push the new enrollment object into the enrollments array
		//objects we push into subdocument array must also follow the schema of the subdocument array in our User model
		user.enrollments.push(newEnrollment);

		//save the changes made to our user document and return the value of saving our document.
		//If we properly saved our document isUserUpdated will contain the boolean true.
		//If we catch an error, isUserUpdated will contain the error message.
		return user.save().then(user => true).catch(err => err.message)

	})

	//if isUserUpdated contains the boolean true, then, the saving of your user document is successful.
	//console.log(isUserUpdated);
	//if isUserUpdated does not contain the boolean true, we will stop our process and return a res.send() to our client with our message.

	if(isUserUpdated !== true) {
		return res.send({message: isUserUpdated})
	}

	//Find the course we will push our enrollee in then send return true as the value of isCourseUpdated. If isCourseUpdated is not equal to true, we will send the error message to our client. If it is, we will check both isUserUpdated and isCourseUpdated and return message.
	let isCourseUpdated  = await Course.findById(req.body.courseId).then(course => {

		//console.log(course);

		//create an object which will details of our employees:
		let enrollee = {
			userId: req.user.id
		}

		//push the enrollee into the enrollees subdocument array of our course:
		course.enrollees.push(enrollee);

		//Save the course document and our update
		//Return true as value of isCourseUpdated if save properly.
		//Return the err.message as value of isCourseUpdated if we catch an error.
		return course.save().then(course => true).catch(err => err.message)
	})

	//console.log(isCourseUpdated);

	//stop the process if their was an error saving our course document.
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	//send a message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdate contain the boolean true
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}

}



module.exports.enroll = async (req,res) => {

	console.log(req.user.id)
	console.log(req.body.courseId)

	if(req.user.isAdmin) {
		return res.send("Action Forbidden")
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(err => err.message)

	})

	if(isUserUpdated !== true) {
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated  = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message)
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}

}

module.exports.getEnrollments = (req,res) => {

	User.findById(req.user.id)
	.then(user => res.send(user.enrollments))
	.catch(err => res.send(err));

}