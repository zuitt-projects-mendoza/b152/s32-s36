const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

//routes

//create/add course
//Since this route should not be accessed by all users, we have to authorize only some logged in users who have the proper authority:
//First, verify if the token the user is using is legit.
router.post('/',verify,verifyAdmin,courseControllers.addCourse);

//get all courses
router.get('/',courseControllers.getAllCourses);

//get single course
router.get('/getSingleCourse/:id',courseControllers.getSingleCourse);

//update course
router.put('/:id',verify,verifyAdmin,courseControllers.updateCourse);

//archive
router.put('/archive/:id',verify,verifyAdmin,courseControllers.archive);

//activate
router.put('/activate/:id',verify,verifyAdmin,courseControllers.activate);

//get active courses
router.get('/getActiveCourses',courseControllers.getActiveCourses);

/*

	Mini-Activity

		Create a course route and controller which is able to get/retrieve all the inactive courses from our db.
			-Then, send the inactive courses into our client
			-Catch the possible error and send it to our client.
		This route can only be used by a registered admin user.

		Screenshot the response in your client and send it to our hangouts

*/

//get inactive courses
router.get('/getInactiveCourses',verify,verifyAdmin,courseControllers.getInactiveCourses)

/*
	Mini-Activity

	Create a new Post route and controller which is able to look for documents with the same name.

		-The request should have a body which contains the name of the course we are looking for.
		-Then, check the result. 
			IF the result is an empty array, or the result.length is 0, then send a message to the client:"No Course found."
			ELSE, send the details of the course.
		-Catch, the error that may possible occur and send the error to the client.
		-Similar to email exists.

			Stretch Goal:
				Use a query operator to make your query case insensitive
					-Research how to use $regex in mongoose

	Create a new Post route and controller which is able to look for documents with the same price.
	
		-The request should have a body which contains the price of the course we are looking for.
		-Then, check the result. 
			IF the result is an empty array, or the result.length is 0, then send a message to the client:"No Course found."
			ELSE, send the details of the course.
		-Catch, the error that may possible occur and send the error to the client.
		-Similar to email exists.

*/

//find courses by name
router.post('/findCoursesByName',courseControllers.findCoursesByName);


//find courses by price
router.post('/findCoursesByPrice',courseControllers.findCoursesByPrice);

router.get('/getEnrollees/:id',verify,verifyAdmin,courseControllers.getEnrollees)

module.exports = router;