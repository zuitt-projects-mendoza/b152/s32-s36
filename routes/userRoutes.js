const express = require("express");
const router = express.Router();

//import user controllers
const userControllers = require("../controllers/userControllers")
//userControllers is a module we imported. In JS, modules are objects.
//exported controllers are methods to their modules.
//console.log(userControllers)

//We will import auth module so we can use our verify and verifyAdmin method as middleware for our routes.
const auth = require("../auth");

//destructure verify and verifyAdmin from auth. Auth, is an imported module, so therefore it is an object in JS.
const {verify,verifyAdmin} = auth;

//console.log(verify);

//User Registration:
router.post("/",userControllers.registerUser);

/*
	Mini-Activity

	Create a route and controller which is able to get all user documents.

	The route's endpoint should be '/'

	Go back to your userController.js and create a new controller called getAllUsers
		-This controller should be able to use the User model and its find() method.
		-Then, send the result to our client.
		-Catch the error and send it to our client.

	You may use our previous notes and repos for this route.

	If you are done, send the proper request from your postman and screenshot the response.
	Send the screenshot in the hangouts.


*/

router.get('/',userControllers.getAllUsers);

//user login
router.post('/login',userControllers.loginUser);

//get logged in users details
//verify method from auth will be used as a middleware. Middleware are functions we can use around our app. In the context of expressjs route, middleware are functions we add in the route and can receive the request, response and next objects. We can have more than 1 middleware before the controller.
//router.get('<endpoint>',middleware,controller);
//When a route has middlewares and controller, we will run through the middleware first before we get to the controller.
//Note: Routes that have verify as a middleware would require us to pass a token from postman.
router.get('/getUserDetails',verify,userControllers.getUserDetails);

router.post('/checkEmailExists',userControllers.checkEmailExists);


/*
	Mini-Activity:

	1. Create a route and controller which will be able to update a logged in user's firstName,lastName and mobileNo.
		-endpoint: '/updateUserDetails'
		-This route should be able to receive a token and verify its legitimacy and decode the token.
		-Create a controller which will enable the logged in user to update his firstName,lastName and mobileNo.
		-You cannot get the id from a url params, however, after verify middleware, the req object now has a property that contains your user's details.
		-use a query from our User model which will allow us to find the document by its id and apply our updates.
		-Then send the updated User's details to our client.
		-Catch the error and send it to our client.

*/

router.put('/updateUserDetails',verify,userControllers.updateUserDetails)

/*
	Mini-Activity

	Create a user route and controller which is able to capture an id from its url using route params. 
		-This route will be used to update a regular user to an admin
		-Only an admin can access this route.
		-In your controller, show the id in the terminal.
		-Screenshot of the captured id in the terminal and send in the hangouts.

*/
router.put('/updateAdmin/:id',verify,verifyAdmin,userControllers.updateAdmin)

//enroll our registered users
//Because you are using the verify middleware, we are supposed to pass a token.
//Doing so, allows us to verify the legitimacy of our token AND get the details of our user from the token and save it in our req.user
router.post('/enroll',verify,userControllers.enroll);

router.get('/getEnrollments',verify,userControllers.getEnrollments)

module.exports = router;